package listarraylistcarddeck;
import java.util.*;
public class MyDeck {
   private static final List<MyCard> PROTOLIST = new ArrayList<MyCard>();

   // Initialize prototype deck
   static {
      for (Suit suit : Suit.values())
         for (Rank rank : Rank.values())
               PROTOLIST.add(new MyCard(rank, suit));
   }

   public static ArrayList<MyCard> newDeck() {
      return new ArrayList<MyCard>(PROTOLIST); // Return copy of prototype deck
   }

   public static void main(String[] args) {
      System.out.println(newDeck());

   }
}

