package org.example.listarraylistcarddeck;

import listarraylistcarddeck.Rank;
import listarraylistcarddeck.Suit;

public class MyCard {

    private final Rank rank;
    private final Suit suit;
    public MyCard(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Rank rank()
    { 
    	return rank; 
    	}

	public Suit suit() {
		return suit; 
		}
    public String toString() {
    	return rank() + " of " + suit();
    	}

}


