package maphashmap;

@SuppressWarnings("serial")
public class StudentNotFoundExeption extends Throwable {
	
	private String message;

	public StudentNotFoundExeption(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}

