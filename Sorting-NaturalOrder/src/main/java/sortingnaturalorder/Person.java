package sortingnaturalorder;

import java.util.Comparator;

public class Person{
	private String personID;
	private String personName;
	private int personAge;
	public Person() {
		super();
	}
	public Person(String personID, String personName, int personAge) {
		super();
		this.personID = personID;
		this.personName = personName;
		this.personAge = personAge;
	}
	public String getPersonID() {
		return personID;
	}
	public void setPersonID(String personID) {
		this.personID = personID;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public int getPersonAge() {
		return personAge;
	}
	public void setPersonAge(int personAge) {
		this.personAge = personAge;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "ID: "+getPersonID()+" Name: "+getPersonName()+" Age: "+getPersonAge();
	}
	/*
	 * public int compare(Person o1, Person o2) { // TODO Auto-generated method stub
	 * return (o1.getPersonAge()<o2.getPersonAge() ? 1 :
	 * (o1.getPersonAge()==o2.getPersonAge() ? 0 : -1)); }
	 */

}
