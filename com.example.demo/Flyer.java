package com.example.demo;

public interface Flyer {
	
	public String takeOff();
	public String land();
	public String fly();

}

