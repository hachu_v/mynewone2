package org.example1.static_final_maven_1demo;
import static org.example1.static_final_maven_1demo.Elevator.goDown;
import static org.example1.static_final_maven_1demo.Elevator.goUp;
import java.util.Scanner;

public class App {
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Elevator elevator = new Elevator();

		System.out.println("Provide operator name: ");
		elevator.setOperatorName(scanner.next());
		int choice = -1;
		do {
			System.out.println("1. Go Up.");
			System.out.println("2. Go Down.");
			System.out.println("3. Display Elevator Details.");
			System.out.println("0. Exit from elevator system.");
			System.out.println("Enter your choice--->");
			choice = scanner.nextInt();
			switch (choice) {
			case 1:
				System.out.print("Provide Floor Number: ");
				goUp(scanner.nextInt());
				break;
			case 2:
				System.out.print("Provide Floor Number: ");
				goDown(scanner.nextInt());
				break;
			case 3:
				System.out.println(elevator.getDetails());
				break;
			case 0:
				System.out.println("Bye!");
				System.exit(0);
				break;

			default:
				break;
			}

		} while (choice != -0);

	}
}
