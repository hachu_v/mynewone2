package org.example1.static_final_maven_1demo;


	
	import java.util.UUID;

	public class Elevator {
		//instance variable
		private String elevatorID;
		private String operatorName;
		private static int CURRENT_FLOOR;
		//class variable
		private static  final int MIN_FLOOR=0;
		private static final int MAX_FLOOR=9;
		
		static
		{
			CURRENT_FLOOR=0;
		}
		
		{
			String temp[]=UUID.randomUUID().toString().split("-");
			elevatorID=temp[0];
		}

		public String getOperatorName() {
			return operatorName;
		}

		public void setOperatorName(String operatorName) {
			this.operatorName = operatorName;
		}

		public String getElevatorID() {
			return elevatorID;
		}
		
		
		public String getDetails()
		{
			return "printing elevator details\n=========================="
					+ "\nElevator ID: "+getElevatorID()+" Operator Name: "+getOperatorName();
		}
		
		
		public static void goUp(int floor)
		{
			if(CURRENT_FLOOR>=MAX_FLOOR)
			{
				System.out.println("Already on Top Floor Can not go up.");
			}
			else if(CURRENT_FLOOR<MIN_FLOOR)
			{
				System.out.println("sorry can not go up!");
			}
			else
			{
				CURRENT_FLOOR=CURRENT_FLOOR+floor;
				System.out.println("Goinng up! Reached flore is :"+CURRENT_FLOOR);
			}
		}
		
		public static void goDown(int floor)
		{
			if(CURRENT_FLOOR<=MIN_FLOOR)
			{
				System.out.println("Already on Ground Floor Can not go down.");
			}
			else if(CURRENT_FLOOR>MAX_FLOOR)
			{
				System.out.println("sorry can not go down!");
			}
			else
			{
				CURRENT_FLOOR=CURRENT_FLOOR-floor;
				System.out.println("Goinng down! Reached floor is :"+CURRENT_FLOOR);
			}
		}
		
		
		
		
		
	}



